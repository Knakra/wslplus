import sys
import os
import subprocess
from ruamel.yaml import YAML
import argparse

class WSLPlus:
    def __init__(self):
        self.wsl_data_root = None
        self.load_config()

    def load_config(self):
        with open("config.yml") as f:
            yaml = YAML()
            config = yaml.load(f)
            self.wsl_data_root = config['wsl_data_root']

    def clone(self, source_distro=None, new_distro_name=None):
        if source_distro is None or new_distro_name is None:
            source_distro, new_distro_name = self.prompt_for_clone_details()

        tmp_export_path = os.path.join(os.getenv('TEMP'), f"{source_distro}_export.tar")

        print(f"Creating {new_distro_name} from {source_distro} at {self.wsl_data_root}...")
        if subprocess.call(f"wsl --export {source_distro} {tmp_export_path}", shell=True) != 0:
            print(f"Failed to export {source_distro}.")
            return

        if subprocess.call(f"wsl --import {new_distro_name} {self.wsl_data_root}{os.sep}{new_distro_name} {tmp_export_path}", shell=True) != 0:
            print(f"Failed to import {new_distro_name}.")
            os.remove(tmp_export_path)
            return

        print("Cleaning up export file...")
        os.remove(tmp_export_path)

        print(f"{new_distro_name} has been successfully created.")
        print("Info: To configure default user put it in /etc/wsl.conf in the new distro.")

        if input("Do you want to launch now? [y/n]: ").strip().lower() == 'y':
            subprocess.call(f"wsl -d {new_distro_name}", shell=True)

    def prompt_for_clone_details(self):
        distros = self.get_distros()
        if not distros:
            print("No WSL distros found. Please check your WSL installation.")
            sys.exit(1)

        for index, distro in enumerate(distros, start=1):
            print(f"{index}. {distro}")
        try:
            choice = int(input("Select the source distro by index: ")) - 1
            if choice < 0 or choice >= len(distros):
                raise ValueError("Invalid index selected.")
        except ValueError as e:
            print(f"Error: {e}. Please enter a valid index.")
            sys.exit(1)

        new_distro_name = input("Enter the new distro name: ").strip()
        return distros[choice], new_distro_name

    def get_distros(self):
        try:
            raw_output = subprocess.check_output("wsl --list --quiet", shell=True)
            decoded_output = raw_output.decode('utf-8').replace('\x00', '')
            distros = [line.strip() for line in decoded_output.split('\n') if line.strip()]
            return distros
        except subprocess.CalledProcessError as e:
            print(f"Failed to list WSL distros: {e}")
            sys.exit(1)

    @staticmethod
    def list_distros():
        print("Current Distro List:")
        subprocess.call("wsl --list", shell=True)

def parse_args():
    parser = argparse.ArgumentParser(description="WSLPlus: A tool for managing WSL distros.")
    subparsers = parser.add_subparsers(dest='command')

    subparsers.add_parser('list', help='List WSL distros')

    clone_parser = subparsers.add_parser('clone', help='Clone a WSL distro')
    clone_parser.add_argument('source_distro', type=str, help='Source distro name', nargs='?')  # Optional
    clone_parser.add_argument('new_distro_name', type=str, help='New distro name', nargs='?')  # Optional

    return parser.parse_args()

def main():
    args = parse_args()
    wsl_plus = WSLPlus()

    if args.command == 'clone':
        wsl_plus.clone(args.source_distro, args.new_distro_name)
    elif args.command == 'list':
        WSLPlus.list_distros()
    else:
        print("Unknown command. Use --help for usage information.")

if __name__ == "__main__":
    main()
