# wslplus

A simple tool/wrapper to enhance the functionality of WSL by allowing one-command operations not directly supported by WSL.
Or even automating more complex tasks. For not it has only the clone feauture.

## Fetaures

| Feature | Description |
| --- | --- |
| `list` | List all WSL Distros |
| `clone` | Clone a WSL Distro under a new name |

## TODO
Make this a full wrapper to use wsl commands
